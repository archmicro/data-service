package models

import "gorm.io/gorm"

type Entry struct {
	gorm.Model
	Content string
}
