package database

import (
	"data-service/models"
	"fmt"
	"log"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

func Connect() *gorm.DB {
	pass := os.Getenv("DB_PASSWORD")
	dsn := fmt.Sprintf(
		"host=postgres-service user=postgres password=%s dbname=micro port=5432 sslmode=disable",
		pass,
	)
	var err error
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Database connection failed: %v", err)
	}
	db.AutoMigrate(&models.Entry{})
	return db
}
