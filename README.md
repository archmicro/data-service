# Data microservice
The main node for working with the database and caching system.
Processes gRPC requests and data from the "post-msg" topic. Sends
asynchronous update messages to API Gateway via Apache Kafka. This
microservice is under development.