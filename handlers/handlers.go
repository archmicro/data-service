package handlers

import (
	"context"
	"data-service/database"
	"data-service/kafka"
	"data-service/models"
	"encoding/json"
	"errors"
	"log"
	"os"
	"time"

	pb "data-service/protos"

	"github.com/IBM/sarama"
	_ "github.com/joho/godotenv/autoload"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
)

var (
	db          *gorm.DB
	ConnRedis   *redis.Client
	sysProducer sarama.AsyncProducer
	usrProducer sarama.AsyncProducer
	highload    kafka.Topic
	post        kafka.Topic
	updates     kafka.Topic
	notices     kafka.Topic
	postMsg     = make(chan []byte)
)

func init() {
	// Database connection
	db = database.Connect()
}

func Kafka() {
	sysProducer = kafka.NewProd()
	usrProducer = kafka.NewProd()
	highload.Fill(os.Getenv("HIGHLOAD"))
	post.Fill(os.Getenv("POST"))
	updates.Fill(os.Getenv("UPDATES"))
	notices.Fill(os.Getenv("NOTICES"))
	go post.Consume(postMsg)
	go postHandle(db)
}

type HandlerService struct{}

func (p *HandlerService) Path(
	ctx context.Context, req *pb.StringRequest, rsp *pb.BytesResponse,
) error {
	var err error
	switch req.Message {
	case "/get":
		log.Printf("Request: %v", req.Message)
		rsp.Reply, err = getData(db)
		if err != nil {
			log.Println(err)
			return err
		}
		return nil
	case "/get-highload":
		log.Printf("Request: %v", req.Message)
		val, err := ConnRedis.Get(context.Background(), "results").Result()
		if err == nil {
			rsp.Reply = []byte(val) // response data
		} else {
			rsp.Reply = []byte(
				`[{"Date":"0001-01-01T00:00:00Z","Message":"No results"}]`,
			)
			return err
		}
		return nil
	case "/delete-all":
		// Temporary handler
		var entries []models.Entry
		db.Find(&entries)
		db.Unscoped().Delete(&entries)
		err := ConnRedis.Del(context.Background(), "entries").Err()
		if err != nil {
			log.Printf("Failed to delete 'entries' from Redis cache: %v", err)
		}
		rsp.Reply = []byte("All entries was deleted")
		return nil
	default:
		log.Printf("Request: %v", req.Message)
		rsp.Reply = []byte("unknown")
		return errors.New("unknown request")
	}
}

func getData(db *gorm.DB) ([]byte, error) {
	ctx := context.Background()
	var entries []models.Entry
	var reply []byte
	i := 1
	//_ = connRedis.Del(ctx, "entries").Err()
REDIS: // Redis transaction loop
	for {
		// Start optimistic locking
		err := ConnRedis.Watch(ctx, func(tx *redis.Tx) error {
			// Get data from Redis
			val, err := tx.Get(ctx, "entries").Result()
			if err == nil {
				reply = []byte(val) // response data
				log.Printf("Data from Redis in %v loop", i)
				return nil
			}

			//time.Sleep(5 * time.Second) // race condition test

			// If key "entries" not exist, get data from DB
			if err := db.Find(&entries).Error; err != nil {
				log.Printf("Failed to get records: %v", err)
				return err
			}

			// Serializing for Redis
			jsonData, err := json.Marshal(entries)
			if err != nil {
				log.Printf("Serializing to JSON failed: %v", err)
				return err
			}
			log.Printf("Data from DB in %v loop", i)
			reply = []byte(jsonData) // response data

			// MULTI command to execute the transaction
			_, err = tx.TxPipelined(
				ctx,
				func(pipe redis.Pipeliner) error {
					expiration := 30 * time.Minute
					pipe.Set(ctx, "entries", jsonData, expiration)
					return nil
				},
			)
			switch {
			case err == redis.TxFailedErr:
				// Another client modified "entries" before we could EXEC
				// Retry the loop to get the updated data and try again
				return err
			case err != nil:
				log.Printf("Unexpected GET loop error: %v", err)
				return err
			default:
				log.Printf("Saved 'entries' in Redis cache in %v loop", i)
				return nil
			}
		}, "entries")
		switch {
		case err == redis.TxFailedErr:
			i++
			continue
		case err != nil:
			return []byte("server_error"), err
		default:
			break REDIS // Break the loop if transaction was successful
		}
	}
	return reply, nil
}

func postHandle(db *gorm.DB) {
	ctx := context.Background()
	for {
		var entry models.Entry
		err := json.Unmarshal(<-postMsg, &entry)
		if err != nil {
			log.Printf("JSON deserializing failed: %v", err)
		}
		if err = db.Create(&entry).Error; err != nil {
			log.Printf("Failed to create record: %v", err)
			notices.Produce([]byte("Data not saved"), usrProducer)
			continue
		}
		i := 1
	REDIS: // Redis transaction loop
		for {
			// Start optimistic locking
			err := ConnRedis.Watch(ctx, func(tx *redis.Tx) error {
				// Get old data from Redis
				oldData, err := tx.Get(ctx, "entries").Result()
				if err != nil && err != redis.Nil {
					log.Printf("Failed to get old record: %v", err)
					return err
				}

				//time.Sleep(5 * time.Second) // race condition test

				var entries []models.Entry
				if oldData != "" {
					// If the value "entries" already exists in Redis, unpack it
					err := json.Unmarshal([]byte(oldData), &entries)
					if err != nil {
						log.Printf("JSON deserializing failed: %v", err)
						return err
					}
				}

				// Add a new entry to the "entries" slice
				entries = append(entries, entry)

				// Serializing for Redis
				jsonData, err := json.Marshal(entries)
				if err != nil {
					log.Printf("Serializing to JSON failed: %v", err)
					return err
				}

				// MULTI command to execute the transaction
				_, err = tx.TxPipelined(
					ctx,
					func(pipe redis.Pipeliner) error {
						expiration := 30 * time.Minute
						pipe.Set(ctx, "entries", jsonData, expiration)
						return nil
					},
				)
				switch {
				case err == redis.TxFailedErr:
					// Another client modified "entries" before we could EXEC
					// Retry the loop to get the updated data and try again
					return err
				case err != nil:
					log.Printf("Unexpected POST loop error: %v", err)
					return err
				default:
					log.Printf("Updated Redis 'entries' in %v loop", i)
					return nil
				}
			}, "entries")
			switch {
			case err == redis.TxFailedErr:
				i++
				continue
			case err != nil:
				log.Printf("Unexpected POST loop error: %v", err)
				i++
				continue
			default:
				break REDIS // Break the loop if transaction was successful
			}
		}
		updates.Produce([]byte{1}, sysProducer)
	}
}
