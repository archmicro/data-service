package main

import (
	"context"
	"data-service/handlers"
	"data-service/kafka"
	"log"
	"os"

	pb "data-service/protos"

	_ "github.com/joho/godotenv/autoload"
	"github.com/redis/go-redis/v9"
	"go-micro.dev/v4"
)

var (
	sysExch kafka.Topic
	userMsg kafka.Topic
)

func main() {
	// Redis
	redisClient := redis.NewClient(&redis.Options{
		Addr: "redis-service:6379",
	})
	_, err := redisClient.Ping(context.Background()).Result()
	if err != nil {
		log.Fatalf("Redis connection failed: %v", err)
	}
	handlers.ConnRedis = redisClient

	// Start Kafka
	sysExch.Fill(os.Getenv("SYS_EXCH"))
	userMsg.Fill(os.Getenv("USER_MSG"))
	topics := kafka.Topics{
		sysExch,
		userMsg,
	}
	kafka.Start(os.Getenv("AK_ADDR"), topics)
	handlers.Kafka()

	// Go Micro server
	service := micro.NewService(
		micro.Name("data-service"),
		micro.Address(":9090"),
	)
	service.Init()
	alloc := new(handlers.HandlerService)
	pb.RegisterExchangeServiceHandler(service.Server(), alloc)
	log.Fatal(service.Run())
}
