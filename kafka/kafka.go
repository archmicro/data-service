package kafka

import (
	"log"
	"strconv"
	"strings"

	"github.com/IBM/sarama"
)

var (
	address []string
)

func Start(addr string, topics Topics) {
	address = strings.Split(addr, ",")
	topics.Create()
}

type Topics []Topic

func (args Topics) Create() {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	client, err := sarama.NewClient(address, config)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	defer client.Close()
	admin, err := sarama.NewClusterAdminFromClient(client)
	if err != nil {
		log.Fatalf("Failed to create admin from client: %v", err)
	}
	defer admin.Close()
	for _, v := range args {
		topicDetail := &sarama.TopicDetail{
			NumPartitions:     v.Part,
			ReplicationFactor: v.Repl,
		}
		topicName := v.Name
		err = admin.CreateTopic(topicName, topicDetail, false)
		if err != nil {
			log.Printf("Topic creating info: %s", err)
		} else {
			log.Printf("Topic '%s' created.", topicName)
		}
	}
}

type Topic struct {
	Name string
	Part int32
	Repl int16
}

func (arg *Topic) Fill(topic string) {
	params := strings.Split(topic, ",")
	arg.Name = params[0]
	part, err := strconv.Atoi(params[1])
	if err != nil {
		log.Fatalf("Invalid partition number in %s: %v", arg.Name, err)
	}
	arg.Part = int32(part)
	repl, err := strconv.Atoi(params[2])
	if err != nil {
		log.Fatalf("Invalid replication factor in %s: %v", arg.Name, err)
	}
	arg.Repl = int16(repl)
}

func (arg Topic) Consume(data chan []byte) {
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	consumer, err := sarama.NewConsumer(address, config)
	if err != nil {
		log.Fatalf("Failed to create consumer: %v", err)
	}
	reader, err := consumer.ConsumePartition(
		arg.Name, arg.Part, sarama.OffsetNewest,
	)
	if err != nil {
		log.Fatalf("Failed to create ConsumePartition %s: %v", arg.Name, err)
	}
	defer reader.Close()
	log.Printf("Awaiting data from %s...", arg.Name)
	for {
		select {
		case msg := <-reader.Messages():
			data <- msg.Value
			log.Printf("Received message from: %s", arg.Name)
		case err := <-reader.Errors():
			log.Printf("%s error consuming message: %v", arg.Name, err)
		}
	}
}

func NewProd() sarama.AsyncProducer {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Partitioner = sarama.NewManualPartitioner
	config.Producer.Return.Successes = true
	client, err := sarama.NewClient(address, config)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	producer, err := sarama.NewAsyncProducerFromClient(client)
	if err != nil {
		log.Fatalf("Failed to create producer from client: %v", err)
	}
	return producer
}

func (arg Topic) Produce(val []byte, prod sarama.AsyncProducer) string {
	message := &sarama.ProducerMessage{
		Topic:     arg.Name,
		Value:     sarama.ByteEncoder(val),
		Partition: arg.Part,
	}
	prod.Input() <- message
	select {
	case success := <-prod.Successes():
		log.Printf(
			"Message sent to partition %d at offset %d",
			success.Partition,
			success.Offset,
		)
		return "Message sent successfully"
	case err := <-prod.Errors():
		log.Printf("Failed to sent message: %v", err)
		return "Message sent unsuccessfully"
	}
}
